var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var pug = require('pug');

app.set('view engine', 'pug');

var messages = [{
    author: "Carlos",
    text: "Hola! que tal?"
},{
    author: "Pepe",
    text: "Muy bien! y tu??"
},{
    author: "Paco",
    text: "Genial!"
}];

app.use(express.static('public'));

app.get('/', function (req, res) {
    //res.status(200).send('Julian');
    res.status(200).render('index');
    //res.status(200).render('index');
    //console.log('res', res);
});
// Emitir evento
io.on('connection', function (socket) {
    console.log('Un cliente se ha conectado con sockets.');
    socket.emit('messages', messages);
    // Recibir evento
    socket.on('new-message', function (data) {
        messages.push(data);
        io.sockets.emit('messages', messages); // io.sockets.emit Notifica a todos los sockets conectados
    });
});

server.listen(8005, function(){
    console.log('Corriendo en el puerto 8005');
});